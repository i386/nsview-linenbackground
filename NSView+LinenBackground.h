//
//  NSView+LinenBackground.h
//
//  Created by James Dumay on 13/03/13.
//  Copyright (c) 2012 Atlassian
//  Copyright (c) 2013 James Dumay
//

#import <Cocoa/Cocoa.h>

@interface NSView (LinenBackground)

-(void)drawLinen;

@end
