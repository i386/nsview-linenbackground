//
//  NSView+LinenBackground.m
//
//  Created by James Dumay on 13/03/13.
//  Copyright (c) 2012 Atlassian
//  Copyright (c) 2013 James Dumay
//

#import "NSView+LinenBackground.h"

@implementation NSView (LinenBackground)

-(void)drawLinen
{
    [[NSGraphicsContext currentContext] saveGraphicsState];
    [[NSGraphicsContext currentContext] setPatternPhase:NSMakePoint(0,[self frame].size.height)];
    
    NSBundle *bundle = [NSBundle bundleWithIdentifier:@"com.apple.AppKit"];
    NSURL * url = [bundle URLForResource:@"NSTexturedFullScreenBackgroundColor" withExtension:@"png"];
    
    NSImage *anImage = [[NSImage alloc] initWithContentsOfURL:url];
    [[NSColor colorWithPatternImage:anImage] set];
    
    NSRectFill([self bounds]);
    [[NSGraphicsContext currentContext] restoreGraphicsState];
}

@end
